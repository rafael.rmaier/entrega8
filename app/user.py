from http import HTTPStatus
from json.decoder import JSONDecodeError
import os
from json import load, dump
from flask import make_response


def list_users():
    file = os.path.join('app', 'database', 'database.json')
    os.system(f'touch {file}')
    try:
        with open(file, "r") as database:
           database_as_dict = load(database)
           return database_as_dict
    except JSONDecodeError:
        with open(file, "w") as database:
            empty_list = []
            dump(empty_list, database)    
        with open(file, "r") as database:
           database_as_dict = load(database)
           return database_as_dict


def create_user(name: str, email: str):
    file = os.path.join('app', 'database', 'database.json')
    with open(file, "r") as database:
        list_users = load(database)
        validate(name, email, list_users=list_users)
        new_user = {"id": len(list_users) + 1, "name": name.title(), "email": email.lower()}
    list_users.append(new_user)
    with open(file, 'w') as database:
        dump(list_users, database)
    return new_user


def validate(*args, list_users):
    types = {
        str: "string",
        int: "integer",
        float: "float",
        list: "list",
        dict: "dictionary",
        bool: "boolean",
    }
    wrong_types_list = []
    name, email = args
    for arg in args:
        if type(arg) != str:
            wrong_types_list.append({f"{arg}": f"{types[type(arg)]}"})
    if wrong_types_list:
        raise WrongAttributeTypeError(wrong_types_list)
    for user in list_users:
        if email.lower() in user['email']:
            raise AttributeError()


class WrongAttributeTypeError(Exception):

    def __init__(self, wrong_types_list) -> None:
        self.message = {
            "wrong fields": wrong_types_list
        }
        super().__init__(self.message)
