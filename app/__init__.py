from flask import Flask, make_response, jsonify, request
from .user import list_users, create_user, WrongAttributeTypeError
from http import HTTPStatus


app = Flask(__name__)

@app.get('/user')
def get_users():
    return {"data": list_users()}, HTTPStatus.OK

@app.post('/user')
def post_user():
    list_users()
    json_data = request.json
    
    try:
        name = json_data['name']
        email = json_data['email']
        new_user = create_user(name, email)
    except WrongAttributeTypeError as err:
        return err.message, HTTPStatus.BAD_REQUEST
    except AttributeError:
        return {"message": "E-mail already exists"}, HTTPStatus.CONFLICT
    except KeyError:
        return {"message": "Incorrect keys. Must be 'name' and 'email'"}, HTTPStatus.BAD_REQUEST
    return {"data": new_user}, HTTPStatus.CREATED